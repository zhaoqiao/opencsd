%global opencsd_tag 539fea3eabd4ce7574494981cd3d0906cfdc5f18

Name:           opencsd
Version:        1.2.1
Release:        1%{?dist}
Summary:        An open source CoreSight(tm) Trace Decode library

License:        BSD
URL:            https://github.com/Linaro/OpenCSD
Source0:        https://github.com/Linaro/OpenCSD/archive/%{opencsd_tag}.tar.gz

BuildRequires:  patch
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  git
BuildRequires:  make

%description
This library provides an API suitable for the decode of ARM(r)
CoreSight(tm) trace streams.

%package devel
Summary: Development files for the CoreSight(tm) Trace Decode library
Requires: %{name}%{?_isa} = %{version}-%{release}
%description devel
The opencsd-devel package contains headers and libraries needed
to develop CoreSight(tm) trace decoders.

%prep
%setup -q -n OpenCSD-%{opencsd_tag}

%build
cd decoder/build/linux
export CFLAGS="$RPM_OPT_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS"
export LDFLAGS="-pie -z now"
LIB_PATH=%{_lib} make %{?_smp_mflags}


%install
cd decoder/build/linux
PREFIX=%{buildroot}%{_prefix} LIB_PATH=%{_lib} make install DISABLE_STATIC=1 DEF_SO_PERM=755


%check
# no upstream unit tests yet

%files
%license LICENSE
%doc HOWTO.md README.md
%{_libdir}/*so\.*
%{_bindir}/*

%files devel
%doc decoder/docs/prog_guide/*
%{_includedir}/*
# no man files..
%{_libdir}/*so

#------------------------------------------------------------------------------
%changelog
* Thu Mar 21 2024 Michael Petlan <mpetlan@redhat.com> - 1.2.1-1
- Update to upstream 1.2.1
  Related: RHEL-29803

* Fri Jan 21 2022 Michael Petlan <mpetlan@redhat.com> - 1.2.0-2
- Update to upstream 1.2.0 -- fix opencsd_tag
  Related: rhbz#2041578

  * Fri Jan 21 2022 Michael Petlan <mpetlan@redhat.com> - 1.2.0-1
- Update to upstream 1.2.0
  Related: rhbz#2041578

* Tue Dec 14 2021 Michael Petlan <mpetlan@redhat.com> - 1.0.0-5
- Added some linker hardening flags to pass annocheck
  Related: rhbz#2031802

  * Tue Dec 14 2021 Michael Petlan <mpetlan@redhat.com> - 1.0.0-5
- Added gating
  Related: rhbz#2031794

* Tue Dec 07 2021 Michael Petlan <mpetlan@redhat.com> - 1.0.0-4
- Bump NVR, needed for erratum RHBA-2021:85351
  Related: rhbz#2029382

* Mon Aug 09 2021 Mohan Boddu <mboddu@redhat.com> - 1.0.0-3
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 1.0.0-2
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Fri Apr  9 2021 Michael Petlan <mpetlan@redhat.com> - 1.0.0-1
- Rebased to upstream 1.0.0

* Wed Sep 23 2020 Jeremy Linton <jeremy.linton@arm.com> - 0.14.3-1
- Update to upstream 0.14.3

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 0.14.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Wed Jul 08 2020 Jeremy Linton <jeremy.linton@arm.com> - 0.14.1-1
- First opencsd package
